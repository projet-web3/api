var ctx = document.getElementById('myChart').getContext('2d');
var data = document.querySelector('#data-graph-information')
data = JSON.parse(data.dataset.graph);

let typeChart = 'line'

function switchMode(){
    if(typeChart === 'bar'){
        typeChart = 'Line';
    }else{
        typeChart = 'bar';
    }
    console.log(typeChart)
    window.myChart.update();
}
var barChartData = {
    labels: data['dates'],
    datasets: [
        {
            label: 'Nombre de produits',
            borderColor: "#007bff",
            backgroundColor: "#007bff",
            data: data['values'][0],
            fill: false,
            // borderWidth: 1,
        },
        {
            label: 'Nombre de restaurateurs',
            borderColor: "#ffc107",
            backgroundColor: "#ffc107",
            data: data['values'][1],
            fill: false,
            // borderWidth: 1,

        },
        {
            label: 'Nombre de commandes',
            borderColor: "#dc3545",
            backgroundColor: "#dc3545",
            data: data['values'][2],
            fill: false,
            // borderWidth: 1,
        },
        {
            label: 'Nombre de clients',
            borderColor: "#28a745",
            backgroundColor: "#28a745",
            data: data['values'][3],
            fill: false,
            // borderWidth: 1,
        }
    ]
};
window.myChart = new Chart(ctx, {
    type: typeChart,
    data: barChartData,
    options: {
        responsive: true,
        hoverMode: 'index',
        stacked: false,
        title: {
            display: true,
            text: 'Évolution des différentes statistiques en fonction du temps'
        },
        legend: false,
    },
});


