var restaurants = document.querySelector('#coords');
restaurants = JSON.parse(restaurants.dataset.coords);

const distance = 20;

smallIcon = new L.Icon({
    iconUrl: 'https://i.postimg.cc/C1kWDDNN/marker-icon-2x.png',
    iconRetinaUrl: 'https://i.postimg.cc/C1kWDDNN/marker-icon-2x.png',
    iconSize:    [25, 41],
    iconAnchor:  [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    shadowSize:  [41, 41]
});

const element = document.getElementById('map');
let map = L.map(element);

function loadMap(centerLat, centerLng, zoom){
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {   //          http://{s}.tile.osm.org/{z}/{x}/{y}.png
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    const centerView = L.latLng(centerLat, centerLng);
    map.setView(centerView, zoom);
}

function addMarker(lat, lng, text, open = false) {
    const marker = L.marker([lat, lng], { icon: smallIcon });
    if (open) {
        marker.addTo(map).bindPopup(text).openPopup();
    } else {
        marker.addTo(map).bindPopup(text);
    }
}

async function getUserCoords(){
    let lat;
    let lon;
    if (window.navigator &&
        window.navigator.geolocation &&
        window.navigator.geolocation.getCurrentPosition(position => {
            this.geolocationPosition = position;
            lat = this.geolocationPosition.coords.latitude;
            lon = this.geolocationPosition.coords.longitude;
            return showRestaurantsAroundUser([
                lat,
                lon
            ])
        }))
    {
        console.log("Coords found");
    }else{
        try {
            const response = await fetch('http://ip-api.com/json');
            if (response.ok){
                const jsonresponse = await response.json();
                lat = jsonresponse.lat;
                lon = jsonresponse.lon;
            }else{
                lat = 45.74846; // Position sur Lyon
                lon = 4.84671;
            }
            return showRestaurantsAroundUser([
                lat,
                lon
            ])
        }catch (error){
            console.log(error);
            return showRestaurantsAroundUser([
                45.74846,
                4.84671
            ])
        }
    }
}

function calculateDistance(latitude1,longitude1,latitude2,longitude2) {
    const p = Math.PI / 180
    const c = Math.cos;
    let a = 0.5 - c((latitude2 - latitude1) * p)/2 + c(latitude1 * p) * c(latitude2 * p) * (1 - c((longitude2 - longitude1) * p))/2;
    const R = 6371; //  Earth distance in km so it will return the distance in km
    return 2 * R * Math.asin(Math.sqrt(a));
}

function showRestaurantsAroundUser(user_coords){
    map.eachLayer(function(layer){
        map.removeLayer(layer);
    });
    loadMap(user_coords[0],user_coords[1], 10);

    addMarker(user_coords[0],user_coords[1],"Votre Position", true);
    for (let i = 0; i < restaurants.length; i++){
        if (calculateDistance(user_coords[0],user_coords[1], restaurants[i].lat, restaurants[i].lon) < distance){
            // console.log(restaurants[i])
            addMarker(restaurants[i].lat, restaurants[i].lon, "<a href='/restaurant-"+restaurants[i].slug+"'>"+restaurants[i].name+"</a>");
        }
    }
}

loadMap('45.74846', '4.84671', 5);
getUserCoords();
