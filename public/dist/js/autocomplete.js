let searchBarItem = $("#searchbar");
let searchBox = $(".search-box");
let resultItem = $("#result-search");

searchBarItem.keyup(function(e){
    let val = searchBarItem.val();
    if(val.length >= 3 && val.length < 99){
        let newURL = "/auto-complete/"+val;

        $.get({
            url : newURL,
            type : 'GET',
            success : function(data){ // success est toujours en place, bien sûr !
                if(data){
                    $('#result-search li').each(function (){
                        this.remove();
                    })
                    // console.log(data)
                    let maxsize = 5;
                    let i = 0;
                    searchBox.css('border-radius', '25px 25px 0 0');
                    for (category in data){
                        for (subitem in data[category]){
                            if(maxsize > i){
                                // console.log(data[category][subitem])
                                let name = data[category][subitem].name;
                                let slug = data[category][subitem].slug;
                                // let categoryUrl = (category === "category" ? "restaurants" : "restaurant");
                                // let categoryName = (category === "category" ? "Catégorie" : "Restaurant");
                                // resultItem.append('<li class="line-result"><a href="/'+categoryUrl+'-'+slug+'">'+categoryName+' > '+ name +' </a></li>')
                                resultItem.append('<li class="line-result"><a href="/restaurant-'+slug+'">'+ name +' </a></li>')

                                i++;
                            }else{
                                break;
                            }

                        }
                    }
                    resultItem.show()
                    resultItem.css('width', searchBox.width())
                }
            },

        });
    }else{
        resultItem.hide()
        searchBox.css('border-radius', '25px');
    }
});
