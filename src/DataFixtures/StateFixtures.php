<?php


namespace App\DataFixtures;


use App\Entity\State;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class StateFixtures extends Fixture
{

    const STATEPREFIX = "STATE";


    public function load(ObjectManager $manager)
    {
        $states = [
            ["name" => "En attente d'acceptation", "step" => 1],
            ["name" => "En cours de préparation", "step" => 2],
            ["name" => "En cours de livraison", "step" => 3],
            ["name" => "Livrée", "step" => 4],
            ["name" => "Annulée", "step" => 5],
        ];
        $i = 0;
        foreach ($states as $state){
            $newstate = new State();
            $newstate->setName($state['name'])
                ->setStep($state['step']);
            $manager->persist($newstate);

            $this->addReference(self::STATEPREFIX.$i, $newstate);
            $i++;
        }

        $manager->flush();

    }
}
