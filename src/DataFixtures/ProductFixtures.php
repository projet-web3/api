<?php


namespace App\DataFixtures;


use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{

    const PRODUCTPREFIX = "PRODUCT";
    const MAX_PRODUCT = 35;
    const DESCRIPTION = RestaurantFixtures::DESCRIPTION;

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create();
        $faker->addProvider(new \FakerRestaurant\Provider\fr_FR\Restaurant($faker));
     
        for($i = 0; $i < (RestaurantFixtures::RESTAURANT_MAX * self::MAX_PRODUCT); $i++){
            $newproduct = new Product();
            $newproduct->setName($faker->foodName())
                    ->setDescription(self::DESCRIPTION)
                    ->setIsSelling($faker->boolean(80))
                    ->setIsActive($faker->boolean(80))
                    ->setPrice($faker->randomFloat(2,0,35))
                    ->setDateCreated($faker->dateTimeBetween('-1 years'))
                    ->setImage("https://loremflickr.com/150/150/food")
                    ->setRestaurant($this->getReference(RestaurantFixtures::RESTAURANTPREFIX.$faker->numberBetween(CustomerFixtures::CUSTOMER_MAX, RestaurantFixtures::RESTAURANT_MAX+-1)));
            $manager->persist($newproduct);

            $this->addReference(self::PRODUCTPREFIX.$i, $newproduct);
        }

        $manager->flush();

    }

    public function getDependencies()
    {
        return [RestaurantFixtures::class];
    }
}
