<?php


namespace App\DataFixtures;


use App\Entity\Restaurant;
use Faker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class RestaurantFixtures extends Fixture implements DependentFixtureInterface
{
    const RESTAURANT_MAX = 45;
    const RESTAURANTPREFIX = "RESTAURANT";
    
    const DESCRIPTION = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam harum laudantium nulla quae quo quos repellat! Amet ex harum ratione. Aliquam aliquid corporis culpa distinctio dolores dolorum eos et facilis, hic ipsam nostrum obcaecati officia possimus provident quam quas quasi quo repellendus sapiente suscipit? A architecto explicabo ipsam nulla.";
    const IMAGE_URL = "https://loremflickr.com/150/150/food";
    
    
    private $passwordEncoder;
    private $slugger;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder,SluggerInterface $slugger)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->slugger = $slugger;
        
    }

    public function load(ObjectManager $manager)
    {
        
        $faker = Faker\Factory::create();
        
        for($i = CustomerFixtures::CUSTOMER_MAX; $i < self::RESTAURANT_MAX+CustomerFixtures::CUSTOMER_MAX;$i++){
            
             $name = $faker->company;
             
             $newrestaurant = new Restaurant();
             $newrestaurant->setName($name)
                    ->setEmail("contact@".$this->slugger->slug(strtolower($name)).".fr")
                    ->setPhone($faker->regexify('^((\+33|0)(6|7|4)(\d{2}){4}$'))
                    ->setDescription(self::DESCRIPTION)
                    ->setImage(self::IMAGE_URL)
                    ->setCategory($this->getReference(CategoryFixtures::CATEGORYPREFIX.rand(0, CategoryFixtures::MAX_CATEGORY-1)))
                    ->setDateCreated($faker->dateTimeBetween('-1 years'))
                    ->setRoles(['ROLE_RESTAURANT'])
                    ->setIsActive($faker->boolean(90))
                    ->setPassword($this->passwordEncoder->encodePassword($newrestaurant, "password"));

            $manager->persist($newrestaurant);

            $this->addReference(self::RESTAURANTPREFIX.$i, $newrestaurant);
        }

        $manager->flush();
    }


    public function getDependencies()
    {
        return [CategoryFixtures::class];
    }
}
