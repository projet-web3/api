<?php


namespace App\DataFixtures;


use App\Entity\Command;
use App\Entity\CommandLine;
use App\Service\CommandService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommandFixtures extends Fixture implements DependentFixtureInterface
{

    private $commandService;
    
    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;
    }
    
    
    public function load(ObjectManager $manager){
    
//        for($i = 0; $i < 30; $i++){
//
//            $customer = $this->getReference(CustomerFixtures::USERPREFIX."0");
//            $address = $customer->getAddress()[0];
//            $hour = (new \DateTime())->modify('+'.rand(0,7)."hour");
//            $restaurant = $this->getReference(RestaurantFixtures::RESTAURANTPREFIX."5");
//
//            $newCommand = new Command();
//            $newCommand->setRestaurant($restaurant)
//                    ->setAddress($this->commandService->addressToString($address))
//                    ->setCustomer($customer)
//                    ->setEffectiveHour($hour)
//                    ->setEstimatedHour($hour->modify('+1 hour'))
//                    ->setState($this->getReference(StateFixtures::STATEPREFIX."0"));
//
//            $total = 0;
//
//            foreach ($restaurant->getProducts() as $product){
//                if(rand(0,1)){
//                    $commandLine = new CommandLine();
//
//                    $commandLine->setPrice($product->getPrice())
//                        ->setCommand($newCommand)
//                        ->setProduct($product)
//                        ->setQuantity(rand(1,7));
//                    $price = $commandLine->getPrice() * $commandLine->getQuantity();
//                    $total += $price;
//
//
//                    $manager->persist($commandLine);
//                }
//            }
//            $newCommand->setTotal($total);
//
//
//            $manager->persist($newCommand);
//        }
//        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [RestaurantFixtures::class, CustomerFixtures::class, AddressFixtures::class, StateFixtures::class, ProductFixtures::class];
    }
}
