<?php


namespace App\DataFixtures;


use App\Entity\Address;
use App\Entity\Customer;
use App\Service\DeliveryService;
use App\Service\LocalizationService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Proxies\__CG__\App\Entity\Restaurant;
use Faker;

class AddressFixtures extends Fixture implements DependentFixtureInterface
{

    const MAXADDRESS = RestaurantFixtures::RESTAURANT_MAX + CustomerFixtures::CUSTOMER_MAX;
    const ADDRESSPREFIX = "ADDRESS";

    
    private $localizationService;
    
    public function __construct(LocalizationService $localizationService)
    {
         $this->localizationService = $localizationService;
    }
     
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
         
        for($i = 0; $i < self::MAXADDRESS; $i++){
            $prefix = RestaurantFixtures::RESTAURANTPREFIX;
            if($i == 0 || $i == 1){
                 $prefix = CustomerFixtures::USERPREFIX;
            }
            do{
                 $newAddress = new Address();
                 $lat = $faker->latitude($min = 45.68389514260777, $max = 45.816136187242066);
                 $lon = $faker->longitude($min = 4.726856840136868, $max = 5.000141752246243);
                 $result = $this->localizationService->getAddressByCoords($lat, $lon);
            }while(!isset($result['street']));
            
            $newAddress->setCountry("France")
                    ->setCity($result['city'])
                    ->setLat($lat)
                    ->setLon($lon)
                    ->setStreet($result['street'])
                    ->setZipcode($result['postcode'])
                    ->setPerson($this->getReference($prefix.$i));

            $manager->persist($newAddress);

            $this->addReference(self::ADDRESSPREFIX.$i, $newAddress);
        }
        $manager->flush();

    }

    public function getDependencies()
    {
        return [CustomerFixtures::class,RestaurantFixtures::class];
    }
}
