<?php


namespace App\DataFixtures;


use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CustomerFixtures extends Fixture
{

    const USERPREFIX = "USER";
    const CUSTOMER_MAX = 2;

    private $passwordEncoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $customers = [
            ["lastname" => "mostacci", "firstname" => "grégory", "email" => "admin@mail.fr", "phone" => "+33645789512", 'role'=> ['ROLE_ADMIN']],
            ["lastname" => "Opinel", "firstname" => "giratou", "email" => "opinel.giratou@monsite.fr", "phone" => "+33734501295"]
        ];
        
        $i = 0;
        foreach ($customers as $customer){
            $newCustomer = new Customer();
            $newCustomer->setFirstname($customer['firstname'])
                    ->setLastname($customer['lastname'])
                    ->setEmail($customer['email'])
                    ->setPassword($this->passwordEncoder->encodePassword($newCustomer, "password"))
                    ->setPhone($customer['phone']);
                    if(isset($customer['role'])){
                        $newCustomer->setRoles($customer['role']);
                    }
            $manager->persist($newCustomer);

            $this->addReference(self::USERPREFIX.$i, $newCustomer);
            $i++;
        }

        $manager->flush();
    }
}
