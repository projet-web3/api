<?php


namespace App\DataFixtures;


use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class CategoryFixtures extends Fixture
{
    const MAX_CATEGORY = 10;
    const CATEGORYPREFIX = "CATEGORY";
    
    
    public function load(ObjectManager $manager)
    {
        $categories = ["Italien", "Chinois", "Indonésien", "Thaïlandais", "Français", "Américain", "FastFood", "Pizzeria", "Crêperie", "Snack"];
        $faker = Faker\Factory::create();
        
        for($i = 0; $i < self::MAX_CATEGORY; $i++){
             $newCategory = new Category();
             $newCategory->setName($categories[$i])->setImage("https://loremflickr.com/150/150/food");
             $manager->persist($newCategory);
     
             $this->addReference(self::CATEGORYPREFIX.$i, $newCategory);
        }

        $manager->flush();
    }
}
