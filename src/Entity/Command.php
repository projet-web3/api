<?php

namespace App\Entity;

use App\Entity\Traits\HistoricTrait;
use App\Repository\CommandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandRepository::class),
 *
 */
class Command
{

    use HistoricTrait;
    /**
     * @ORM\Column(type="datetime")
     */
    private $estimatedHour;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $effectiveHour;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="commands")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="commands")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    /**
     * @ORM\ManyToOne(targetEntity=State::class, inversedBy="commands")
     * @ORM\JoinColumn(nullable=false)
     */
    private $state;

//    /**
//     * @ORM\ManyToOne(targetEntity=Address::class)
//     * @ORM\JoinColumn(nullable=false,onDelete="SET NULL")
//     */
//    private $address;

    /**
     * @ORM\OneToMany(targetEntity=CommandLine::class, mappedBy="command")
     */
    private $commandLines;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    public function __construct()
    {
        $this->dateCreated = new \DateTime();
        $this->commandLines = new ArrayCollection();
    }

    public function getEstimatedHour(): ?\DateTimeInterface
    {
        return $this->estimatedHour;
    }

    public function setEstimatedHour(\DateTimeInterface $estimatedHour): self
    {
        $this->estimatedHour = $estimatedHour;

        return $this;
    }

    public function getEffectiveHour(): ?\DateTimeInterface
    {
        return $this->effectiveHour;
    }

    public function setEffectiveHour(\DateTimeInterface $effectiveHour): self
    {
        $this->effectiveHour = $effectiveHour;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): self
    {
        $this->state = $state;

        return $this;
    }
//
//    public function getAddress(): ?Address
//    {
//        return $this->address;
//    }
//
//    public function setAddress(?Address $address): self
//    {
//        $this->address = $address;
//
//        return $this;
//    }

    /**
     * @return Collection|CommandLine[]
     */
    public function getCommandLines(): Collection
    {
        return $this->commandLines;
    }

    public function addCommandLine(CommandLine $commandLine): self
    {
        if (!$this->commandLines->contains($commandLine)) {
            $this->commandLines[] = $commandLine;
            $commandLine->setCommand($this);
        }

        return $this;
    }

    public function removeCommandLine(CommandLine $commandLine): self
    {
        if ($this->commandLines->removeElement($commandLine)) {
            // set the owning side to null (unless already changed)
            if ($commandLine->getCommand() === $this) {
                $commandLine->setCommand(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }
}
