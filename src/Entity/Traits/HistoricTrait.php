<?php


namespace App\Entity\Traits;


use Doctrine\ORM\Mapping as ORM;

trait HistoricTrait
{
    use IdTrait;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateModificated;


    /**
     * @return mixed
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated): self
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateModificated()
    {
        return $this->dateModificated;
    }

    /**
     * @param mixed $dateModificated
     */
    public function setDateModificated($dateModificated): self
    {
        $this->dateModificated = $dateModificated;
        return $this;
    }
}
