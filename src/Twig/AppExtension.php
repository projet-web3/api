<?php

namespace App\Twig;

use App\Service\DeliveryService;
use App\Service\LocalizationService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $kernelProjectDir;
    private $deliveryService;

    public function __construct(string $kernelProjectDir, DeliveryService $deliveryService)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        $this->deliveryService = $deliveryService;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('fileExist', [$this, 'assetsExist']),
//            new TwigFunction('getDistance', [$this, 'calculDistance']),
        ];
    }

    public function assetsExist($asset){
        return file_exists($this->kernelProjectDir."/public/".$asset);
    }
    
//    public function calculDistance($coords1, $coords2){
//         $this->deliveryService->calculateDistance($coords1, $coords2);
//    }
    
}
