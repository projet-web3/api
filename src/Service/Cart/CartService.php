<?php


namespace App\Service\Cart;


use App\Entity\Restaurant;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{

    private $session;
    private $productRepository;

    public function __construct(SessionInterface $session, ProductRepository $productRepository)
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    public function add($id){

        $restaurant_id = $this->session->get('cart_restaurant', "");
        $product_restaurant_id = $this->productRepository->find($id)->getRestaurant()->getId();
        if($restaurant_id != $product_restaurant_id){
            $this->reset();
            $this->session->set('cart_restaurant', $product_restaurant_id);
        }
        $panier = $this->session->get('cart', []);

        if(!empty($panier[$id])){
            $panier[$id]++;
        }else{
            $panier[$id] = 1;
        }
        $this->session->set('cart', $panier);
    }



    public function getData(){

        $panier = $this->session->get('cart', []);
        $data = [];

        foreach ($panier as $id => $quantity){
            $data[] = [
                'product' => $this->productRepository->find($id),
                'quantity' => $quantity
            ];
        }
        return $data;
    }

    public function delete(?int $id)
    {
        $panier = $this->session->get('cart', []);
        unset($panier[$id]);
        $this->session->set('cart', $panier);
    }

    public function remove(?int $id)
    {
        $panier = $this->session->get('cart', []);
        $panier[$id]--;
        if($panier[$id] == 0){
            unset($panier[$id]);
        }
        $this->session->set('cart', $panier);
    }

    public function getTotal(){
        $panier = $this->session->get('cart', []);
        $total = 0;
        foreach ($panier as $id => $quantity){
            $product = $this->productRepository->find($id);
            $total += $quantity * $product->getPrice();
            $total += Restaurant::PRICEFDP;

        }
        return $total;
    }

    public function reset()
    {
        $this->session->set('cart', []);
    }
    
    public function isset(){
         $cart = $this->session->get('cart', []);
         if($cart == []){
              return false;
         }else{
              return true;
         }
    }
}
