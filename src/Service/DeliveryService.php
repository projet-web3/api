<?php


namespace App\Service;


use App\Repository\CategoryRepository;
use App\Repository\RestaurantRepository;
use App\Repository\StateRepository;

class DeliveryService
{

    private $categoryrepo;
    private $restaurepo;
    private $staterepo;

    public function __construct(CategoryRepository $categoryRepository, RestaurantRepository $restaurantRepository, StateRepository $staterepo)
    {
        $this->categoryrepo = $categoryRepository;
        $this->restaurepo = $restaurantRepository;
        $this->staterepo = $staterepo;
    }

    public function getRandomCategory(){
        $categories = $this->categoryrepo->findAll();
        if(sizeof($categories) > 0){
            return $categories[rand(0, sizeof($categories)-1)];
        }
        return false;
    }

    public function getRandomRestaurants($city){

        $category = $this->getRandomCategory();
        if($category){
            do{
                $category = $this->getRandomCategory();
                $restaurants = $this->restaurepo->findRestaurantByAddressAndCategory($category, $city);
                
            }while(sizeof($restaurants) == 0 );
//            $restaurants = $this->restaurepo->findBy(['category' => $category, "name" => $city]);
            return [
                "category" => $category,
                "city" => $city,
                "restaurants" => $restaurants
            ];
        }
        return false;
//        throw new \Exception("Il n'y a pas de catégories définies");
    }

    public function getStatebyStep($step){
        return $this->staterepo->findOneBy(['step' => $step]);
    }

}
