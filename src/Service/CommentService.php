<?php


namespace App\Service;


use App\Entity\Comment;
use App\Entity\Product;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;

class CommentService
{
    
    private $commentRepo;
    public function __construct(EntityManagerInterface $em, CommentRepository $commentRepo)
    {
        $this->em = $em;
        $this->commentRepo = $commentRepo;
    }
    
    public function addComment($product, $content, $user, $rating){
        $comment = new Comment();
        $comment->setProduct($product)
            ->setCustomer($user)
            ->setOpinion($content)
            ->setRating($rating);
        $this->em->persist($comment);
    }
    
    public function commentExist(Product $product, $user)
    {
        $result = $this->commentRepo->findBy(['product' => $product, 'customer' => $user]);
        if(sizeof($result) > 0){
            return true;
        }
        return false;
    }
    
    public function getRating($sizeComments, $ratingProduct, $rating){
        if ($ratingProduct != null) {
            $newRating = round(($ratingProduct * $sizeComments + $rating) / ($sizeComments + 1), 2);
        } else {
            $newRating = $rating;
        }
        return $newRating;
    }
    
}
