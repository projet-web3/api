<?php


namespace App\Service;


use App\Entity\Address;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LocalizationService
{
    
    const API_GOUV_DATA = 'https://api-adresse.data.gouv.fr/search/?q=';
    
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function callApi($query){
        $response = $this->client->request(
            'GET',
            self::API_GOUV_DATA.$query
        );
        return $response->toArray();
    
    }
    
    public function getCoordsByAddress($address)
    {
        $street = $address->getStreet();
        $zipcode = $address->getZipCode();

        $query = $street .'&postcode='. $zipcode;
        $content = $this->callApi($query);
     
        return (isset($content['features'][0]) ? $content['features'][0]: false);

    }

    public function getAddressByCoords($lat, $lon){
         $query = '1&lat='. $lat .'&lon='. $lon;
         $content = $this->callApi($query);
         return (isset($content['features'][0]['properties']) ? $content['features'][0]['properties']: false);
    }
    
    
    public function calculateDistance($coords1, $coords2){
        
        $r = 6371; //rayon de la terre
        $dLat = deg2rad($coords2['lat'] - $coords1['lat']);// diff lat1 et lat2
        $dLon = deg2rad($coords2['lon'] - $coords1['lon']);// diff lon1 et lon2
        
        $a =
            sin($dLat/2) * sin($dLat/2)
            + cos(deg2rad($coords1['lat'])) * cos(deg2rad($coords2['lat'])) *
            sin($dLon/2) * sin($dLon/2)
        ;
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $d = $r * $c;
        
        return $d;
    }
    
    public function convertAddressToCoords(Address $address){
        return [
            'lat' => $address->getLat(),
            'lon' => $address->getLon(),
        ];
    }
    
    public function redefineAddress(Address $address, $data, $person = null){
        $address->setLat($data['geometry']['coordinates'][1])
            ->setLon($data['geometry']['coordinates'][0])
            ->setStreet($data['properties']['housenumber']." ".$data['properties']['street'])
            ->setZipCode($data['properties']['postcode'])
            ->setCity($data['properties']['city']);
        if(isset($person)){
            $address->setPerson($person);
        }
        
        return $address;
    
    }
}
