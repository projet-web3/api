<?php


namespace App\Service;


use Symfony\Component\DependencyInjection\Container;

class FileService
{
    
    private $container;
    
    public function construct(Container $container){
        $this->container = $container;
    }
    public function moveFile($file){

        $filename = md5(uniqid()) . '.' . $file->guessExtension();
        $file->move(
//            $this->container->getParameter('upload_dir'),
            'uploads/',
            $filename
        );
        return $filename;
    }
}
