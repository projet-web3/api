<?php


namespace App\Service\Admin;


use App\Repository\CommandRepository;
use App\Repository\CustomerRepository;
use App\Repository\ProductRepository;
use App\Repository\RestaurantRepository;

class HistoricService
{
    private $productRepo;
    private $commandRepo;
    private $restaurantRepo;
    private $customerRepo;

    public function __construct(
        RestaurantRepository $restaurantRepository,ProductRepository $productRepository,
        CommandRepository $commandRepository,CustomerRepository $customerRepository
    )
    {
        $this->commandRepo = $commandRepository;
        $this->restaurantRepo = $restaurantRepository;
        $this->productRepo = $productRepository;
        $this->customerRepo = $customerRepository;

    }

    public function getHistoric()
    {
        $data[] = $this->refactorArray($this->productRepo->findProductHistoric());
        $data[] = $this->refactorArray($this->restaurantRepo->findRestaurantHistoric());
        $data[] = $this->refactorArray($this->commandRepo->findCommandHistoric());
        $data[] = $this->refactorArray($this->customerRepo->findCustomerHistoric());
        return $data;
    }

    public function triDate($data){
        // On récupère toutes les dates
        foreach($data as $item){
            $table = array_keys($item);
            foreach ($table as $subitem){
                $dateList[] = $subitem;
            }
        }
        $dateList = array_unique($dateList);
        usort($dateList, function ($a, $b){return strtotime($a) - strtotime($b);} );
        return $dateList;
    }

    public function checkData($listDate, $data){
        foreach ($listDate as $onedate){
            $i = 0;
            foreach ($data as $item){
                if(isset($item[$onedate])){
                    $datagraph[$i][]= $item[$onedate];
                }else{
                    $datagraph[$i][] = 0;
                }
                $i++;
            }
        }
        return $datagraph;
    }

    public function refactorArray($array){
        $newarray = [];
        foreach ($array as $subarray){
            $date = $subarray['year']."/".$subarray['month']."/".$subarray['day'];
            $newarray[$date] = $subarray['quantite'];
        }
        return $newarray;
    }

    public function graphValue(){
        $data = $this->getHistoric();
        $dateTri = $this->triDate($data);
    
        $graphData = $this->checkData($dateTri, $data);
        return [
            "dates" => $dateTri,
            "values" => $graphData
        ];
    }
}
