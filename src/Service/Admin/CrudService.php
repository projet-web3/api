<?php


namespace App\Service\Admin;


use App\Data\ConstData;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CrudService
{

    private $request;
    private $formFactory;
    private $adminService;
    private $container;
    private $em;

    public function __construct(
        RequestStack $requestStack,
        FormFactoryInterface $formFactory,
        AdminService $adminService,
        ContainerInterface $container,
        EntityManagerInterface $em
    )
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->formFactory = $formFactory;
        $this->adminService = $adminService;
        $this->container = $container;
        $this->em = $em;
    }

    public function getData($id = NULL): array
    {
        $data['type'] = $this->adminService->getUrlType(1);
        $data['instance'] = ConstData::getInstanceType($data['type']);
        $repo = $this->container->get('doctrine')->getRepository($data['instance']["class"]);
        if($id){
            $data['object'] = $repo->find($id);
            $data['form'] = $this->formFactory->create($data['instance']['formtype'], $data['object']);
        }else{
            $data['object'] = $data['instance']['object'];
            $data['form'] = $this->formFactory->create($data['instance']['formtype'], $data['object']);
        }
        return $data;
    }

    public function checkData($form,$type,$object,$id = NULL): void
    {
        if(in_array($type, ['category','restaurant','product']) ){
            $file = $form->get('image')->getData();
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                $this->container->getParameter('upload_dir'),
                $filename
            );
            $object->setImage($filename);

        }
        if($id){
            $this->container->get('doctrine')->getManager()->flush();
        }else{
            $entityManager = $this->container->get('doctrine')->getManager();
            $entityManager->persist($object);
            $entityManager->flush();
        }
    }

}
