<?php


namespace App\Service\Admin;


use App\Entity\Product;
use App\Entity\Restaurant;
use Symfony\Component\HttpFoundation\Session\Session;

class CheckService
{

//    private $session;
//
//    public function __construct(Session $session)
//    {
//        $this->session = $session;
//    }

    public function checkRestaurant(Restaurant $restaurant){
        if($restaurant->getIsActive()){
            return true;
        }
        return false;
    }

    public function checkProduct(Product $product){
        if(
            $product->getIsActive()
            && $product->getIsSelling()
            && checkService::checkRestaurant($product->getRestaurant())
        ){
            return true;
        }
//        $this->session->getFlashBag()->add('danger','Ce produit n\'est pas disponible');
        return false;
    }


}
