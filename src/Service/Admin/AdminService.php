<?php


namespace App\Service\Admin;


use App\Data\ConstData;
use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\Restaurant;
use App\Entity\State;
use App\Form\CategoryType;
use App\Form\CommandType;
use App\Form\CustomerType;
use App\Form\ProductType;
use App\Form\RestaurantType;
use App\Form\StateType;
use App\Repository\CategoryRepository;
use App\Repository\CommandRepository;
use App\Repository\CustomerRepository;
use App\Repository\ProductRepository;
use App\Repository\RestaurantRepository;
use App\Repository\StateRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AdminService
{

    private $productRepo;
    private $commandRepo;
    private $restaurantRepo;
    private $customerRepo;
    private $stateRepo;
    private $categoryRepo;
    private $historicService;
    private $requestStack;
    private $container;

    public function __construct(
        RestaurantRepository $restaurantRepository,ProductRepository $productRepository,
        CommandRepository $commandRepository,CustomerRepository $customerRepository,
        CategoryRepository $categoryRepository,StateRepository $stateRepository,
        HistoricService $historicService, RequestStack $requestStack,
        ContainerInterface $container
    )
    {
        $this->commandRepo = $commandRepository;
        $this->restaurantRepo = $restaurantRepository;
        $this->productRepo = $productRepository;
        $this->customerRepo = $customerRepository;
        $this->categoryRepo = $categoryRepository;
        $this->stateRepo = $stateRepository;

        $this->historicService = $historicService;
        $this->requestStack = $requestStack;
        $this->container = $container;

    }

    public function getDataStatistic(){
        $types = ['restaurant', 'product', 'command', 'customer', 'category', 'state'];
        foreach ($types as $type){
            $data = ConstData::getData($type);
            $instance = ConstData::getInstanceType($type);
            $datainfo[$type] = [
                'logo' => $data['logo'],
                'nbr' => $this->container->get('doctrine')->getRepository($instance['class'])->countElement(),
                'name' => $data['sing_fr'],
                'color' => $data['color'],
                'type' => $type
            ];
        }
        return $datainfo;
    }

    public function getGraphInfo(){
        return $this->historicService->graphValue();
    }


    public function getUrlType($nbr){
        return explode("/",trim($this->requestStack->getCurrentRequest()->getRequestUri(), "/"))[$nbr];
    }

}
