<?php

namespace App\Service;

use App\Entity\Command;

class MailingService
{
     
     const FROM = "contact@bringeattome.fr";
     
     public function sendMail($object, $content, $destination){
          mail($destination, $object, $content, 'From: '.self::FROM);
     }
     
     public function mailCommandPassed(Command $command){
          $objectCustomer = "Récapitulatif de votre commande";
          $objectRestorer = "Récapitulatif de la commande";
          
          $addressDelivery = $command->getAddress();
          $total = $command->getTotal();
          $hourEstimated = $command->getEstimatedHour()->format("d-m-Y H:i");
          $restaurant = $command->getRestaurant()->getName();
          $commandId = $command->getId();
          $destEmailCustomer = $command->getCustomer()->getEmail();
          $destEmailRestorer = $command->getRestaurant()->getEmail();
          
          $content = "
               <!DOCTYPE html>
               <html>
                   <body>
                         <h1>Récapitulatif de votre commande:</h1>
                         <p>Numero de commande: $commandId</p>
                         <p>Lieu de livraison: $addressDelivery</p>
                         <p>Restaurant: $restaurant</p>
                         <p>Heure de livraison: $hourEstimated </p>
                         <p>Total: $total</p>
                    </body>
               </html>
          ";
    
         $headers[] = 'MIME-Version: 1.0';
         $headers[] = 'Content-type: text/html; charset=iso-8859-1';
          $this->sendMail($objectCustomer, $content, $destEmailCustomer, implode("\r\n", $headers));
          $this->sendMail($objectRestorer, $content, $destEmailRestorer, implode("\r\n", $headers));
     }
     
}
