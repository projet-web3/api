<?php


namespace App\Service;


use App\Entity\Address;
use App\Entity\Command;
use App\Entity\CommandLine;
use App\Entity\Customer;
use App\Entity\Restaurant;
use App\Repository\AddressRepository;
use App\Service\Cart\CartService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class CommandService
{
 
     private $cartService;
     private $deliveryService;
     private $localizationService;
     private $em;
     private $security;
     private $addressRepo;
     
     public function __construct(
         CartService $cartService, DeliveryService $deliveryService,
         EntityManagerInterface $em, Security $security,
         AddressRepository $addressRepository, LocalizationService $localizationService
     )
     {
          $this->cartService = $cartService;
          $this->deliveryService = $deliveryService;
          $this->localizationService = $localizationService;
          $this->em = $em;
          $this->security = $security;
          $this->addressRepo = $addressRepository;
     }
     
     
     public function makeCommand($idAddress, $customer){
          // Test si panier est rempli
          $cart = $this->cartService->getData();
          
          $restaurant = $cart[0]['product']->getRestaurant();
    
          $coordsRestaurant = $this->localizationService->convertAddressToCoords($restaurant->getAddress()[0]);
          $coordsCustomer = $this->localizationService->convertAddressToCoords($this->addressRepo->findOneBy(["id" => $idAddress]));
          
          $distance = $this->localizationService->calculateDistance($coordsCustomer, $coordsRestaurant);
          $hourEstimated = intval($distance + Restaurant::TIMEMINI);
     
          $dateEstimated = (new \DateTime())->modify("+$hourEstimated minutes");
          $address = $this->addressRepo->findOneBy(["id" => $idAddress]);
          $addressDelivery = $this->addressToString($address);
          
          $command = new Command();
          $command->setTotal($this->cartService->getTotal())
               ->setAddress($addressDelivery)
               ->setCustomer($customer)
               ->setState($this->deliveryService->getStatebyStep(1))
               ->setEstimatedHour($dateEstimated)
               ->setRestaurant($restaurant);
     
          $this->em->persist($command);
     
          foreach ($cart as $item){$this->makeCommandLine($item, $command);}
     
          $this->em->flush();
          return $command;
     
     }
     
     public function makeCommandLine($item, $command){
          $commandLine = new CommandLine();
          $commandLine->setQuantity($item['quantity'])
               ->setProduct($item['product'])
               ->setCommand($command)
               ->setPrice($item['product']->getPrice() * $item['quantity']);
          $this->em->persist($commandLine);
     }
     
     public function changeMoney($total, Restaurant $restaurant, Customer $user){
         $user->setMoney($user->getMoney() - $total);
         $restaurant->setMoney($restaurant->getMoney() + $total);
         $this->em->flush();
    
     }
     
     public function addressToString(Address $address){
         $addressString = $address->getStreet().", ".$address->getCity()." ".$address->getZipcode();
         $addressString .= ($address->getName() != null) ? " | ". $address->getName() : "";
         return $addressString;
     }
     
     
     
}
