<?php

namespace App\Controller;

use App\Entity\Command;
use App\Form\RestaurantType;
use App\Repository\CommandRepository;
use App\Service\DeliveryService;
use App\Service\FileService;
use App\Service\LocalizationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/restorer")
 */
class RestorerController extends AbstractController
{
    /**
     * @Route("/", name="restorer")
     */
    public function index(CommandRepository $commandRepository): Response
    {
        $commands = $this->getUser()->getCommands();
        $quantity = $commandRepository->countCommandsByState($this->getUser());

        return $this->render('restorer/index.html.twig', [
            'controller_name' => 'RestorerController',
            'commands' => $commands,
            'quantity' => $quantity,
        ]);
    }

    /**
     * @Route("/account", name="restorer_account")
     */
    public function account(): Response
    {
        return $this->render('restorer/account.html.twig', [
            'controller_name' => 'RestorerController',
        ]);
    }

    /**
     * @Route("/account/edit", name="edit_restorer_account")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param LocalizationService $localizationService
     * @return Response
     */
    public function editAccount(Request $request, FileService $fileService, LocalizationService $localizationService): Response
    {
        $restaurant = $this->getUser();
        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->remove('password');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $file = $form->get('image')->getData();
            if ($file !== null) {
                $filename = $fileService->moveFile($file);
                $restaurant->setImage($filename);
            }

            $restaurant->setDateModificated(new \DateTime());
            $address = $restaurant->getAddress()[0];

            $data = $localizationService->getCoordsByAddress($address);

            if(!$data || !isset($data['properties']['street'])){
                $this->addFlash('danger', 'Cette adresse n\'existe pas !');
            }else{
                $localizationService->redefineAddress($address, $data);

                $entityManager->flush();
                $this->addFlash('success', 'Vos informations ont bien été changées.');
    
                return $this->redirectToRoute('restorer_account');
            }
        }
        return $this->render('restorer/edit_restorer.html.twig', [
            'form' => $form->createView(),
        ]);
    }
     
     /**
      * @Route("/change-step/{id}", name="change_step")
      */
     public function changeStep(Command $command, DeliveryService $deliveryService, EntityManagerInterface $em): Response
     {
          if($command->getRestaurant() == $this->getUser()){
               $step = $command->getState()->getStep();
               $nextStep = $deliveryService->getStatebyStep($step+1);
               $command->setState($nextStep);
               
               if($step+1 == 4){$command->setEffectiveHour(new \DateTime());}
               
               $em->flush();
          }else{
               $this->addFlash('danger', 'Cette commande n\'existe pas');
          }
          return $this->redirectToRoute("restorer");
     }
     
    /**
     * @Route("/commands", name="all_commands")
     */
    public function allCommands(): Response
    {
        return $this->render('restorer/commands.html.twig', [
            'commands' => $this->getUser()->getCommands()
        ]);
    }

    /**
     * @Route("/command/{id}", name="show_command")
     * @param Command $command
     * @return Response
     */
    public function showCommand(Command $command): Response
    {
        if($command->getRestaurant() == $this->getUser()){
            return $this->render('restorer/show_command.html.twig', [
                'command' => $command
            ]);
        }else{
            $this->addFlash('danger', 'Cette commande n\'existe pas');
        }
        return $this->redirectToRoute("restorer");
    }

}
