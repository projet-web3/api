<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Repository\RestaurantRepository;
use App\Service\Admin\CrudService;
use App\Service\FileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/restorer/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_index", methods={"GET"})
     * @param ProductRepository $productRepository
     * @param Request $request
     * @return Response
     */
    public function index(ProductRepository $productRepository, Request $request): Response
    {
        return $this->render('restorer/product/index.html.twig', [
            'products' => $productRepository->findBy(['restaurant' => $this->getUser()], ['dateCreated' => 'DESC'])
        ]);
    }

    /**
     * @Route("/new", name="product_new", methods={"GET","POST"})
     * @param Request $request
     * @param RestaurantRepository $restaurantRepository
     * @return Response
     */
    public function new(Request $request, RestaurantRepository $restaurantRepository, FileService $fileService): Response
    {
        $product = new Product();
        $product->setRestaurant($restaurantRepository->findOneBy(['id' => $this->getUser()]));
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $fileService->moveFile($form->get('image')->getData());
            $product->setImage($file);
    
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('restorer/product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_show", methods={"GET"})
     * @param Product|null $product
     * @return Response
     */
    public function show(Product $product = null): Response
    {
        if (!empty($product) and $product->getRestaurant() === $this->getUser()){
            return $this->render('restorer/product/show.html.twig', [
                'product' => $product,
            ]);
        }
        return $this->redirectToRoute('product_index');
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Product|null $product
     * @return Response
     */
    public function edit(Request $request, Product $product = null, FileService $fileService): Response
    {
        if (empty($product) or $product->getRestaurant() !== $this->getUser()){
            return $this->redirectToRoute('product_index');
        }
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('image')->getData();
            
            if ($file !== null){
                $filename = $fileService->moveFile($file);
                $product->setImage($filename);
            }

            $product->setDateModificated(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('restorer/product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_delete", methods={"DELETE"})
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function delete(Request $request, Product $product = null): Response
    {
        if (empty($product) or $product->getRestaurant() !== $this->getUser()){
            return $this->redirectToRoute('product_index');
        }

        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $product->setIsActive(false)->setIsSelling(false);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_index');
    }
}
