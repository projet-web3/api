<?php


namespace App\Controller;


use App\Entity\Address;
use App\Entity\Customer;
use App\Entity\Restaurant;
use App\Form\CustomerType;
use App\Form\RestaurantType;
use App\Security\PersonAuthenticator;
use App\Service\FileService;
use App\Service\LocalizationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegisterController extends AbstractController
{
     /**
      * @Route("/register", name="register_customer")
      */
     public function registerCustomer(
         Request $request, UserPasswordEncoderInterface $passwordEncoder,
         GuardAuthenticatorHandler $guardHandler, PersonAuthenticator $authenticator,
         EntityManagerInterface $em
     ){
     
          $customer = new Customer();
          $form = $this->createForm(CustomerType::class, $customer);
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
                
                $customer->setPassword($passwordEncoder->encodePassword($customer,$form->get('password')->getData()));
                $em->persist($customer);
                $em->flush();
                
                return $guardHandler->authenticateUserAndHandleSuccess(
                     $customer,
                     $request,
                     $authenticator,
                     'main' // firewall name in security.yaml
                );
     
               
          }
          return $this->render("security/register_customer.html.twig", [
               "form" => $form->createView()
          ]);
     }
    
    /**
     * @Route("/register-restorer", name="register_restorer")
     */
    public function registerRestorer(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        PersonAuthenticator $authenticator,
        LocalizationService $localizationService,
        FileService $fileService,
        EntityManagerInterface $em
    
    ){
        $address = new Address();
        $restaurant = new Restaurant();
        $restaurant->addAddress($address);
        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        
            $filename = $fileService->moveFile($form->get('image')->getData());
            $restaurant->setImage($filename);
            $data = $localizationService->getCoordsByAddress($address);
    
            if(!$data || !isset($data['properties']['street'])){
                $this->addFlash('danger', 'Cette adresse n\'existe pas !');
            }else{
                $address = $localizationService->redefineAddress($address, $data, $restaurant);

                $restaurant->setRoles(['ROLE_RESTAURANT']);
                $restaurant->setPassword($passwordEncoder->encodePassword($restaurant,$form->get('password')->getData()));
                $em->persist($restaurant);
                $em->persist($address);
    
                $em->flush();
    
                return $guardHandler->authenticateUserAndHandleSuccess(
                    $restaurant,
                    $request,
                    $authenticator,
                    'main' // firewall name in security.yaml
                );
                
            }
            
        }
        return $this->render("security/register_restorer.html.twig", [
            "form" => $form->createView(),
            "error" => ""
        ]);
    }
}
