<?php

namespace App\Controller;

use App\Data\ConstData;
use App\Entity\Address;
use App\Entity\Customer;
use App\Form\AddressType;
use App\Form\CustomerType;
use App\Service\LocalizationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/account")
 */
class CustomerController extends AbstractController
{
    /**
     * @Route("", name="customer_account")
     */
    public function index(): Response
    {
        $modal['title'] = "Etes-vous sûr de vouloir supprimer votre compte ?";
        $modal['content'] = "Si vous supprimez votre compte, il restera actif 7 jours, avant la suppression totale. 
        Vous recevrez un email de confirmation lors de sa suppression.";
        $modal['confirm_button'] = "Je suis certain";

        return $this->render('customer/index.html.twig',[
            "modal" => $modal
        ]);
    }
     
     /**
      * @Route("/add-address", name="add_address")
      */
     public function addAddress(Request $request, EntityManagerInterface $em, LocalizationService $localizationService): Response
     {
          $address = new Address();
          $form = $this->createForm(AddressType::class, $address);
          $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid()){
               $data = $localizationService->getCoordsByAddress($form->getData());
//               dd($data);
     
               if(!$data || !isset($data['properties']['street'])){
                    $this->addFlash('danger', 'Cette adresse n\'existe pas !');
               }else{
                    $form->getData()->setLat($data['geometry']['coordinates'][1]);
                    $form->getData()->setLon($data['geometry']['coordinates'][0]);
                    $form->getData()->setStreet($data['properties']['housenumber']." ".$data['properties']['street']);
                    $form->getData()->setZipCode($data['properties']['postcode']);
                    $form->getData()->setCity($data['properties']['city']);
                    $address->setPerson($this->getUser());
                    $em->persist($address);
                    $em->flush();
                    
                    $this->addFlash('success', 'Votre adresse a bien été crée !');
                    return $this->redirectToRoute('customer_account');
               }
          
          }
          return $this->render('customer/editaddress.html.twig',[
               'form' => $form->createView(),
               "title" => "Créer"
          ]);
     }

    /**
     * @Route("/delete-account", name="delete_account")
     */
    public function delete(EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        $user->setIsActive(false);
        $em->flush();
        $this->addFlash('success', "Votre compte à bien été désactivé. Dans 7 jours, il sera supprimé. Pour tout problème, contactez le support.");
        return $this->redirectToRoute('app_logout');
    }
     
     /**
      * @Route("/delete-address/{id}", name="delete_address")
      */
     public function deleteAddress(Address $address, EntityManagerInterface $em): Response
     {
          if($address->getPerson() != $this->getUser()){
               $this->addFlash('danger', 'Erreur de suppression !');
          }else{
               if(sizeof($this->getUser()->getAddress()->getValues()) <= 1 ){
                    $this->addFlash('danger', 'Vous devez créer une autre adresse avant de supprimer celle-ci.');
               }else{
                    $em->remove($address);
                    $em->flush();
                    $this->addFlash('success', "Votre adresse a bien été supprimée.");
               }
          }
          return $this->redirectToRoute('customer_account');
     }
    

    /**
     * @Route("/edit-account", name="edit_account")
     */
    public function edit(EntityManagerInterface $em, Request $request): Response
    {
        $userform = $this->createForm(CustomerType::class, $this->getUser());
        $userform->remove('password');
        $userform->handleRequest($request);
        if($userform->isSubmitted() && $userform->isValid()){
             $em->flush();
     
             $this->addFlash('success', 'Votre compte a bien été modifié!');
             return $this->redirectToRoute('customer_account');
        }
        return $this->render('customer/editaccount.html.twig',[
            'form' => $userform->createView()
        ]);
    }
     
     /**
      * @Route("/edit-address/{id}", name="edit_address")
      */
     public function editAddress(EntityManagerInterface $em, Address $address, Request $request, LocalizationService $localizationService): Response
     {
          if($address->getPerson() != $this->getUser()){
               $this->addFlash('danger', 'Erreur de modification !');
               $this->redirectToRoute('customer_account');
          }
          $form = $this->createForm(AddressType::class, $address);
          $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid()){
               $data = $localizationService->getCoordsByAddress($form->getData());
               if(!$data || !isset($data['properties']['street'])){
                    $this->addFlash('danger', 'Cette adresse n\'existe pas !');
               }else{
                   $address = $localizationService->redefineAddress($address, $data);
//                    $form->getData()->setLat($data['geometry']['coordinates'][1]);
//                    $form->getData()->setLon($data['geometry']['coordinates'][0]);
//                    $form->getData()->setStreet($data['properties']['housenumber']." ".$data['properties']['street']);
//                    $form->getData()->setZipCode($data['properties']['postcode']);
//                    $form->getData()->setCity($data['properties']['city']);
//                    $em->persist($address);
                    $em->flush();
                    $this->addFlash('success', 'Votre adresse a bien été changé!');
               }
          
          }
          return $this->render('customer/editaddress.html.twig',[
               'form' => $form->createView(),
               "title" => "Créer"
          ]);
     }
    
    
    /**
     * @Route("/detail-command", name="customer_detail")
     */
    public function detail(): Response
    {
        return $this->render('customer/command_detail.html.twig',[
            "commands" => $this->getUser()->getCommands()
        ]);
    }

}
