<?php


namespace App\Controller;

use App\Entity\Command;
use App\Entity\CommandLine;
use App\Entity\Customer;
use App\Entity\Restaurant;
use App\Service\Cart\CartService;
use App\Service\CommandService;
use App\Service\DeliveryService;
use App\Service\MailingService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartController
 * @Route("/command")
 */
class CommandController extends AbstractController
{
     
     /**
      * @Route("/step-address", name="choose_address")
      */
     public function chooseAddress(){

          return $this->render('index/chooseaddress.html.twig');
     }
     
    /**
     * @Route("/valid", name="command_valid")
     */
    public function valid(CartService $cartService, CommandService $commandService, Request $request, MailingService $mailingService): Response
    {
        $addressChoose = $request->request->get('address');
        if($addressChoose == null){
            return $this->redirectToRoute('choose_address');
        }
        $customer = $this->getUser();

        if($cartService->isset()){
             if($customer->getMoney() >= $cartService->getTotal()){
                  $command = $commandService->makeCommand($addressChoose, $customer);
                  $cartService->reset();
                  $this->addFlash('success', "Votre commande a bien été passée.");
                  
                  $commandService->changeMoney($command->getTotal(), $command->getRestaurant(), $customer);
                  $mailingService->mailCommandPassed($command);
                  return $this->redirectToRoute('customer_detail');
             }else{
                  $this->addFlash('danger', "Vous n'avez pas assez d'argent.");
             }
        }else{
             $this->addFlash('danger', "Votre panier est vide.");
        }
        return $this->redirectToRoute('cart_home');

    }

}
