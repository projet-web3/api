<?php


namespace App\Controller;


use App\Repository\RestaurantRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/auto-complete/{slug}", name="autocomplete")
     */
    public function autocomplete($slug, RestaurantRepository $restaurantRepository): Response
    {
        $data['restaurant'] = $restaurantRepository->findlike($slug);
//        dd($data['restaurant']);
    
        return $this->json($data);
    }
    
    /**
     * @Route("/search", name="search_page")
     */
    public function search(RestaurantRepository $restaurantRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $param = $request->query->get('s');
        
        $data['restaurant'] = $restaurantRepository->findlikeAddress($param);
        
        $restaurants = $paginator->paginate($data['restaurant'], $request->query->getInt('page', 1), 10);
        return $this->render('index/searchlist.html.twig', [
            'restaurants' => $restaurants,
            'numberResult' => sizeof($data['restaurant'])
        
        ]);
    }
}
