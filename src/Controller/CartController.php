<?php

namespace App\Controller;

use App\Entity\Product;
use App\Service\Admin\CheckService;
use App\Service\Cart\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartController
 * @Route("/cart")
 */
class CartController extends AbstractController
{

    /**
     * @Route("", name="cart_home")
     */
    public function index(CartService $cartService): Response
    {
        $cartService->getData();

        return $this->render('index/cart.html.twig', [
            'items' => $cartService->getData(),
            'total' => $cartService->getTotal(),
        ]);
    }

    /**
     * @Route("/add-{id}", name="cart_add")
     */
    public function add(CartService $cartService, Product $product, CheckService $checkService): Response
    {
        $returnRoute = $this->redirectToRoute('restaurant_one', ["slug" => $product->getRestaurant()->getSlug()]);
        if($checkService->checkProduct($product)) {
            $cartService->add($product->getId());
            $this->addFlash('success',   "Le produit <b>". $product->getName() . "</b> a bien été ajouté au panier.");

        }else{
            $this->addFlash('danger', 'Ce produit est indisponible.');
        }
        return $returnRoute;
    
    }

    /**
     * @Route("/delete-{id}", name="cart_delete")
     */
    public function delete(CartService $cartService, Product $product): Response
    {
        $this->addFlash('danger', 'Le produit \''.$product->getName().'\' a bien été supprimé du panier.');
        $cartService->delete($product->getId());
        return $this->redirectToRoute('cart_home');
    }

    /**
     * @Route("/remove-{id}", name="cart_remove")
     */
    public function remove(CartService $cartService, Product $product): Response
    {
        $cartService->remove($product->getId());
        return $this->redirectToRoute('cart_home');
    }

    /**
     * @Route("/reset", name="cart_reset")
     */
    public function reset(CartService $cartService): Response
    {
        $cartService->reset();
        return $this->redirectToRoute('cart_home');
    }
}
