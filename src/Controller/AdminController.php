<?php

namespace App\Controller;

use App\Data\ConstData;
use App\Entity\CommandLine;
use App\Entity\Customer;
use App\Service\Admin\AdminService;
use App\Service\Admin\CrudService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin_home")
     */
    public function index(AdminService $adminService): Response
    {
        return $this->render('admin/index.html.twig', [
            "informations" => $adminService->getDataStatistic(),
            "data_graph" => $adminService->getGraphInfo()
        ]);
    }
     
     /**
      * @Route("/credit/{id}", name="admin_credit")
      */
     public function credit(Customer $customer, EntityManagerInterface $em): Response
     {
          $customer->setMoney($customer->getMoney()+400);
          $em->flush();
          return $this->redirectToRoute('type_list', ['slug' => 'customer']);
     }
    
    /**
     * @Route("/{slug}/", name="type_list")
     */
    public function typeList(AdminService $service, PaginatorInterface $paginator, Request $request): Response
    {
        // paramconverter puis \/
//        $instance = new DataStructure(//values précises) builder design pattern
         $type = $service->getUrlType(1);
         $instance = ConstData::getInstanceType($type);
         $data = ConstData::getData($type);
         $allitems = $this->getDoctrine()->getRepository($instance['class'])->findAll();
         $items = $paginator->paginate(
              $allitems, // Requête contenant les données à paginer (ici nos articles)
              $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
              20 // Nombre de résultats par page
         );
     
         return $this->render('admin/list.html.twig', [
               "values" => $items,
               "title" => $data['sing_fr'],
               "columns" => $data['columns'],
               "type" => $type
        ]);
    }

    /**
     * @Route("/{slug}/add", name="admin_add_type")
     */
    public function addType(Request $request, CrudService $crudService, PaginatorInterface $paginator): Response
    {
        $data = $crudService->getData();
        $data['form']->handleRequest($request);
        if($data['form']->isSubmitted() && $data['form']->isValid()){
            $crudService->checkData($data['form'], $data['type'], $data['object']);
            return $this->redirectToRoute("type_list", ["slug" => $data['type']]);
        }

        return $this->render('admin/form.html.twig',[
            "type" => $data['type'],
            "form" => $data['form']->createView(),
            "item" => $data['object'],
            "title" => "Création",
            "data" => ConstData::getData($data['type'])
        ]);
    }

    /**
     * @Route("/{slug}/edit/{id}", name="admin_edit_type")
     */
    public function editType(Request $request, CrudService $crudService, $id){
        $data = $crudService->getData($id);
        $data['form']->handleRequest($request);
        if($data['form']->isSubmitted() && $data['form']->isValid()){
            $crudService->checkData($data['form'], $data['type'], $data['object']);
            return $this->redirectToRoute("type_list", ["slug" => $data['type']]);
        }
        return $this->render('admin/form.html.twig',[
            "type" => $data['type'],
            "form" => $data['form']->createView(),
            "item" => $data['object'],
            "data" => ConstData::getData($data['type']),
            "title" => "Modification"
        ]);
    }

    /**
     * @Route("/{slug}/delete/{id}", name="admin_delete_type")
     */
    public function deleteType(AdminService $adminService, EntityManagerInterface $em, $id){
        $type= $adminService->getUrlType(1);

        $instance = ConstData::getInstanceType($type);

        $repo = $this->getDoctrine()->getRepository($instance["class"]);
        $item = $repo->find($id);
        $em->remove($item);
        $em->flush();

        return $this->redirectToRoute("type_list", ["slug" => $type]);
    }

    /**
     * @Route("/{slug}/{id}", name="admin_detail_one_type")
     */
    public function oneDetailType(AdminService $adminService, $id, PaginatorInterface $paginator, Request $request){
        $type = $adminService->getUrlType(1);
        $instance = ConstData::getInstanceType($type);
        $data = ConstData::getData($type);
        $repo = $this->getDoctrine()->getRepository($instance['class']);
        $item = $repo->findBy(['id' => $id]);
     
         $items = $paginator->paginate(
              $item, // Requête contenant les données à paginer (ici nos articles)
              $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
              20 // Nombre de résultats par page
         );
        
        return $this->render('admin/list.html.twig', [
            "values" => $items,
            "title" => $data['sing_fr'],
            "columns" => $data['columns'],
            "type" => $type
        ]);
    }

    /**
     * @Route("/{slug}/reverse-display/{id}", name="admin_display_type")
     */
    public function display(AdminService $adminService, $id): Response
    {
        $type = $adminService->getUrlType(1);
        $instance = ConstData::getInstanceType($type);
        $repo = $this->getDoctrine()->getRepository($instance['class']);
        $object = $repo->find($id);
        $object->setIsActive(!$object->getIsActive());
        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($_SERVER['HTTP_REFERER']);
//        return $this->redirectToRoute('type_list', ['slug' => $type]);
    }

    /**
     * @Route("/{slug_parent}/{id}/{slug_child}", name="admin_detail_type")
     */
    public function detailType(AdminService $adminService, $id, PaginatorInterface $paginator, Request $request){
        $type = $adminService->getUrlType(1);
        $subtype = $adminService->getUrlType(3);

        $instance = ConstData::getInstanceType($subtype);
        $data = ConstData::getData($subtype);
        if($subtype == "product" && $type == "command"){
            $repo = $this->getDoctrine()->getRepository(CommandLine::class);
            $item = $repo->findDetail($id);
        }else{
            $repo = $this->getDoctrine()->getRepository($instance["class"]);
            $item = $repo->findDetail($type, $id);
        }
     
         $items = $paginator->paginate(
              $item, // Requête contenant les données à paginer (ici nos articles)
              $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
              20 // Nombre de résultats par page
         );
        
        return $this->render('admin/list.html.twig', [
            "values" => $items,
            "title" => $data['sing_fr'],
            "columns" => $data['columns'],
            "type" => $subtype,
            "subtype"=> $type
        ]);
    }
     

}
