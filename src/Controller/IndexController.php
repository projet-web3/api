<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Comment;
use App\Entity\Product;
use App\Entity\Restaurant;
use App\Repository\CategoryRepository;
use App\Repository\CommandLineRepository;
use App\Repository\ProductRepository;
use App\Repository\RestaurantRepository;
use App\Service\Admin\CheckService;
use App\Service\CommentService;
use App\Service\DeliveryService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    
    /**
     * @Route("/", name="home")
     */
    public function index(CategoryRepository $categoryRepository, RestaurantRepository $restaurantRepository, DeliveryService $deliveryService): Response
    {

        $categories = $categoryRepository->findBy([], [], 4);
        $newRestaurants = $restaurantRepository->findBy(['isActive' => true], ['dateCreated' => 'DESC'], 2);
        $coordsJs = $restaurantRepository->getCoords();
        
        $deliveryInfo = $deliveryService->getRandomRestaurants("Lyon");
        $deliveryInfo['pricemin'] = Restaurant::PRICEFDP;
        $deliveryInfo['timemin'] = Restaurant::TIMEMINI;
        $deliveryInfo['pricekm'] = Restaurant::PRICEKILOMETERS;
        
        return $this->render('index/index.html.twig', [
            'categories' => $categories,
            'new_restaurants' => $newRestaurants,
            'coords' => $coordsJs,
            'delivery_info' => $deliveryInfo
        
        ]);
    }
    
    /**
     * @Route("/restaurants-{slug}", name="category_one")
     */
    public function category(Category $category, RestaurantRepository $restaurantRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $restaurantsAll = $restaurantRepository->findBy(['isActive' => true, "category" => $category]);
        
        $restaurants = $paginator->paginate($restaurantsAll, $request->query->getInt('page', 1), 10);
        
        return $this->render('index/category.html.twig', [
            'restaurants' => $restaurants,
            'category' => $category
        ]);
    }
    
    /**
     * @Route("/restaurant-{slug}", name="restaurant_one")
     */
    public function restaurant(Restaurant $restaurant, CheckService $checkService, ProductRepository $productRepository, PaginatorInterface $paginator, Request $request): Response
    {
        if (!$checkService->checkRestaurant($restaurant)) {
            $this->addFlash('danger', 'Le restaurant est indisponible temporairement, voici d\'autres restaurants du même type');
            return $this->redirectToRoute('category_one', ["slug" => $restaurant->getCategory()->getName()]);
        }
        
        $productsAll = $productRepository->findBy(['isActive' => true, 'isSelling' => true, 'restaurant' => $restaurant]);
        
        $products = $paginator->paginate($productsAll, $request->query->getInt('page', 1), 9);
        
        return $this->render('index/restaurant.html.twig', [
            'products' => $products,
            'restaurant' => $restaurant,
            'size' => sizeof($productsAll)
        ]);
    }
    
    /**
     * @Route("/category-all", name="all_category")
     */
    public function getAllCategory(CategoryRepository $categoryRepository): Response
    {
        return $this->render('index/allcategory.html.twig', [
            'categories' => $categoryRepository->findAll()
        ]);
    }
    
    /**
     * @Route("/product-detail/{slug}", name="product_detail")
     */
    public function productDetail(Product $product, Request $request, EntityManagerInterface $em, ProductRepository $productRepository, CommandLineRepository $commandLineRepository, CommentService $commentService): Response
    {
        $rating = $request->request->get('rating');
        if ($rating != null) {
            $content = ($request->request->get('comment') != null ? $request->request->get('comment') : null);
            $commentService->addComment($product, $content, $this->getUser(), $rating);
    
            $product->setRating($commentService->getRating(sizeof($product->getComments()), $product->getRating(), $rating));
            $em->flush();
            return $this->redirectToRoute('product_detail', ['slug' => $product->getSlug()]);
            
        }
        $products = $productRepository->findBy(['restaurant' => $product->getRestaurant(), 'isActive' => true, "isSelling" => true], ['name' => "DESC"], 4);
        $key = array_search($product, $products);
        unset($products[$key]);
        $alreadyBought = [];
        if ($this->getUser()->getRoles() !== 'ROLE_RESTAURANT'){
            $commandLineBought = $commandLineRepository->findProductAlreadyBought($this->getUser());
            foreach ($commandLineBought as $line){
                $alreadyBought[] = $line->getProduct();
            }
        }
        return $this->render('index/productdetail.html.twig', [
            "product" => $product,
            "productlink" => $products,
            "alreadywrite" => $commentService->commentExist($product, $this->getUser()),
            "alreadyBought" => $alreadyBought
        ]);
    }
    
    
    /**
     * @Route("/cancel-command/{id}", name="cancelCommand")
     */
    public function cancelCommand(Command $command, DeliveryService $deliveryService, EntityManagerInterface $em): Response
    {
        $nextStep = $deliveryService->getStatebyStep(5);
        $command->setState($nextStep);
        $user = $this->getUser();
        $user->setMoney($user->getMoney()+$command->getTotal()*0.75);

        $em->flush();
        return $this->redirectToRoute("customer_detail");
    }
}
