<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('country', TextType::class, ["label" => 'Pays'])
            ->add('city', TextType::class, ["label" => 'Ville'])
            ->add('street', TextType::class, ["label" => 'Numéro et nom de rue'])
            ->add('zipcode', TextType::class, ["label" => 'Code postale'])
            ->add('name', TextType::class, ["label" => 'Nom (facultatif)','required' => false])
//            ->add('lat')
//            ->add('lon')
     
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
