<?php


namespace App\Data;


use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\Restaurant;
use App\Entity\State;
use App\Form\CategoryType;
use App\Form\CommandType;
use App\Form\CustomerType;
use App\Form\ProductType;
use App\Form\RestaurantType;
use App\Form\StateType;

class ConstData
{
    const RESTAURANT = ["logo" => "<i class='fas fa-user fa-2x'></i>", "color" => "warning", "sing_fr" => "restaurant", "type" => "restaurant", "plur_en" => "restaurants",
        "columns" => ['Nom', "Email", "Téléphone", "Image", 'Catégorie', 'Nombre de produit', "Date d'inscription", 'Nombre de commande']];
    const PRODUCT = ["logo" => "<i class=\"fas fa-hamburger fa-2x\"></i>", "color" => "primary", "sing_fr" => "produit", "type" => "product", "plur_en" => "products",
        "columns" => ['Nom','Prix', "Image",'Description', 'En vente', 'Restaurant']];
    const CATEGORY = ["logo" => "<i class=\"fas fa-user fa-2x\"></i>", "color" => "info", "sing_fr" => "categorie", "type" => "category", "plur_en" => "categories",
        "columns" => ['Nom', "Nombre de restaurant", "Image"]];
    const STATE = ["logo" => "<i class=\"fas fa-user fa-2x\"></i>", "color" => "secondary", "sing_fr" => "état", "type" => "state", "plur_en" => "states",
        "columns" => ['Valeur', "Nombre de commandes associées"]];
    const CUSTOMER = ["logo" => "<i class=\"fas fa-user fa-2x\"></i>", "color" => "success", "sing_fr" => "client", "type" => "customer", "plur_en" => "customers",
        "columns" => ['Nom Prénom', "Email", "Téléphone", 'Nombre de commandes', "Date d'inscription"]];
    const COMMAND = ["logo" => "<i class=\"fas fa-shopping-cart fa-2x\"></i>", "color" => "danger", "sing_fr" => "commande", "type" => "command", "plur_en" => "commands",
        "columns" => ['Heure estimée','Heure effective','Client', 'Restaurant', 'Adresse', 'Nombre de produits', 'Total']];

    public static function getData($type){
        if ($type == self::RESTAURANT['type']) return self::RESTAURANT;
        if ($type == self::PRODUCT['type']) return self::PRODUCT;
        if ($type == self::CATEGORY['type']) return self::CATEGORY;
        if ($type == self::STATE['type']) return self::STATE;
        if ($type == self::CUSTOMER['type']) return self::CUSTOMER;
        if ($type == self::COMMAND['type']) return self::COMMAND;
    }

    public static function getInstanceType($type){
        $array = [
            "category" => ["formtype" => CategoryType::class, "object" => new Category(), "class" => Category::class],
            "state" => ["formtype" => StateType::class, "object" => new State(), "class" => State::class],
            "restaurant" => ["formtype" => RestaurantType::class, "object" => new Restaurant(), "class" => Restaurant::class],
            "customer" => ["formtype" => CustomerType::class, "object" => new Customer(), "class" => Customer::class],
            "command" => ["formtype" => CommandType::class, "object" => new Command(), "class" => Command::class],
            "product" => ["formtype" => ProductType::class, "object" => new Product(), "class" => Product::class],
        ];
        return $array[$type];
    }
}
