<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function countElement()
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findProductHistoric(){
        return $this->createQueryBuilder('p')
            ->select('count(p) as quantite, MONTH(p.dateCreated) as month, DAY(p.dateCreated) as day, YEAR(p.dateCreated) as year')
            ->groupBy('month')
            ->addGroupBy('day')
            ->addGroupBy('year')
            ->getQuery()
            ->getResult();
    }

    public function findDetail($type, $value){
        return $this->createQueryBuilder('p')
            ->where('p.'.$type.' = :rule')
            ->setParameter('rule', $value)
            ->getQuery()
            ->getResult();
    }
     
     public function findlike($name)
     {
          return $this->createQueryBuilder('p')
               ->select('p.id, p.name, p.slug')
               ->where('p.name LIKE :name')
               ->setParameter(':name', '%'.$name.'%')
               ->getQuery()
               ->getResult();
     }
}
