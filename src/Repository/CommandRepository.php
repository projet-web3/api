<?php

namespace App\Repository;

use App\Entity\Command;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Command|null find($id, $lockMode = null, $lockVersion = null)
 * @method Command|null findOneBy(array $criteria, array $orderBy = null)
 * @method Command[]    findAll()
 * @method Command[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Command::class);
    }

    public function countElement()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findCommandHistoric(){
        return $this->createQueryBuilder('c')
            ->select('count(c) as quantite, MONTH(c.dateCreated) as month, DAY(c.dateCreated) as day, YEAR(c.dateCreated) as year')
            ->groupBy('month')
            ->addGroupBy('day')
            ->addGroupBy('year')
            ->getQuery()
            ->getResult();
    }

    public function findDetail($type, $value){
        return $this->createQueryBuilder('c')
            ->where('c.'.$type.' = :rule')
            ->setParameter('rule', $value)
            ->getQuery()
            ->getResult();
    }
    
    public function countCommandsByState($restaurant)
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.state), s.step, s.name')
            ->innerJoin('c.state', 's', Join::WITH, 's = c.state')
            ->groupBy('c.state')
            ->where('c.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant)
            ->getQuery()
            ->getResult();
    }
}
