<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\AbstractPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AbstractPerson|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractPerson|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractPerson[]    findAll()
 * @method AbstractPerson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{

    //SELECT * FROM `person` INNER JOIN customer ON person.id = customer.id;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractPerson::class);
    }
}
