<?php

namespace App\Repository;

use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    public function countElement()
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findRestaurantByAddressAndCategory($category, $city){
        return $this->createQueryBuilder('r')
            ->where('r.category = :category')
            ->setParameter('category', $category)
            ->join('r.address', 'a')
            ->andWhere('a.city = :city')
            ->setParameter('city', $city)
            ->andWhere("r.isActive = 1")
            ->setMaxResults(2)
            ->getQuery()
            ->getResult()
            ;
    }

    public function getCoords(){
        return $this->createQueryBuilder('r')
            ->select('r.id, r.name, a.lat, a.lon, r.slug')
            ->join('r.address', 'a', Join::WITH)
            ->getQuery()
            ->getResult();
    }


    public function findRestaurantHistoric(){
        return $this->createQueryBuilder('r')
            ->select('count(r) as quantite, MONTH(r.dateCreated) as month, DAY(r.dateCreated) as day, YEAR(r.dateCreated) as year')
            ->groupBy('month')
            ->addGroupBy('day')
            ->addGroupBy('year')
            ->getQuery()
            ->getResult();
    }

    public function findDetail($type, $value){
        return $this->createQueryBuilder('r')
            ->where('r.'.$type.' = :rule')
            ->setParameter('rule', $value)
            ->getQuery()
            ->getResult();
    }

    public function findlike($name)
    {
        return $this->createQueryBuilder('r')
            ->select('r.name, r.id, r.slug')
            ->where('r.name LIKE :name')
            ->setParameter(':name', "%$name%")
            ->andWhere('r.isActive = 1')
            ->getQuery()
            ->getResult();
    }
    
    public function findlikeAddress($name)
    {
        return $this->createQueryBuilder('r')
//            ->select('r.name, r.id, r.slug, r.image, a.street')
            ->where('r.name LIKE :name')
            ->setParameter(':name', "%$name%")
            ->andWhere('r.isActive = 1')
            ->getQuery()
            ->getResult();
    }
    
}
