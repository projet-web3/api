<?php

namespace App\Repository;

use App\Entity\CommandLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CommandLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommandLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommandLine[]    findAll()
 * @method CommandLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommandLine::class);
    }

    public function findDetail($value){
        return $this->createQueryBuilder('cl')
            ->select('cl, p')
            ->join('cl.product', 'p', Join::WITH, 'cl.product = p.id')
            ->join('cl.command', 'c', Join::WITH, 'cl.command = c.id')
            ->where('c.id = :rule')
            ->setParameter('rule', $value)
            ->getQuery()
            ->getResult();
    }

    public function findProductAlreadyBought($user)
    {
        return $this->createQueryBuilder('cl')
            ->select('cl')
            ->join('cl.command', 'c')
            ->where('c.customer = :user')
            ->setParameter(':user', $user)
            ->getQuery()
            ->getResult();
    }
}
