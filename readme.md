# BringEatToMe - Web project

- `composer install`
- create .env.local and write `DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7` with `db_user`, `db_password` and `db_name` corresponding to your database infos.
- `php bin/console doctrine:database:create`
- `php bin/console make:migration`
- `php bin/console doctrine:migrations:migrate`
- `php bin/console doctrine:fixtures:load`
- `php bin/console server:run`
